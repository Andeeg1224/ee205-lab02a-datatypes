///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 02a - Datatypes
///
/// @file int.h
/// @version 1.0
///
/// Print the characteristics of the "int" and "unsigned int" datatypes.
///
/// @author Andee Gary  <andeeg@hawaii.edu>
/// @brief  Lab 02 - Datatypes - EE 205 - Spr 2021
/// @date   22_01_2021
///////////////////////////////////////////////////////////////////////////////

extern void doint();            /// Print the characteristics of the "int" datatype
extern void flowint();          /// Print the overflow/underflow characteristics of the "int" datatype


extern void doUnsignedint();    /// Print the characteristics of the "unsigned int" datatype
extern void flowUnsignedint();  /// Print the overflow/underflow characteristics of the "unsigned int" datatype

