///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 02a - Datatypes
///
/// @file long.h
/// @version 1.0
///
/// Print the characteristics of the "long" and "unsigned long" datatypes.
///
/// @author Andee Gary  <andeeg@hawaii.edu>
/// @brief  Lab 02 - Datatypes - EE 205 - Spr 2021
/// @date   22_01_2021
///////////////////////////////////////////////////////////////////////////////

extern void dolong();            /// Print the characteristics of the "long" datatype
extern void flowlong();          /// Print the overflow/underflow characteristics of the "long" datatype


extern void doUnsignedlong();    /// Print the characteristics of the "unsigned long" datatype
extern void flowUnsignedlong();  /// Print the overflow/underflow characteristics of the "unsigned long" datatype

